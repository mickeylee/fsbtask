//
//  Person+CoreDataProperties.h
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//
//

#import "Person+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Person (CoreDataProperties)

+ (NSFetchRequest<Person *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSData *avatarData;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) int height;
@property (nonatomic) int mass;
@property (nullable, nonatomic, copy) NSString *hairColor;
@property (nullable, nonatomic, copy) NSString *skinColor;
@property (nullable, nonatomic, copy) NSString *eyeColor;
@property (nonatomic) int birthYear;
@property (nullable, nonatomic, copy) NSString *gender;

@end

NS_ASSUME_NONNULL_END
