//
//  Person+CoreDataProperties.m
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//
//

#import "Person+CoreDataProperties.h"

@implementation Person (CoreDataProperties)

+ (NSFetchRequest<Person *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Person"];
}

@dynamic avatarData;
@dynamic name;
@dynamic height;
@dynamic mass;
@dynamic hairColor;
@dynamic skinColor;
@dynamic eyeColor;
@dynamic birthYear;
@dynamic gender;

@end
