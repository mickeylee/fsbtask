//
//  DatabaseManager.m
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import "DatabaseManager.h"

static DatabaseManager *sharedInstance = nil;

@implementation DatabaseManager

+ (void)initialize {
    if (sharedInstance == nil) {
        sharedInstance = [[DatabaseManager alloc] init];
    }
}

+ (DatabaseManager *)sharedInstance {
    if (sharedInstance == nil) {
        [self initialize];
    }
    
    return sharedInstance;
}


#pragma mark - Person Entity

- (Person *)createPersonEntityFrom:(NSDictionary *)dict {
    NSManagedObjectContext *context = [[self persistentContainer] viewContext];
    
    Person *person = [NSEntityDescription insertNewObjectForEntityForName: @"Person" inManagedObjectContext: context];
    person.name = dict[@"name"];
    person.height = [dict[@"height"] intValue];
    person.mass = [dict[@"mass"] intValue];
    person.hairColor = dict[@"hair_color"];
    person.skinColor = dict[@"skin_color"];
    person.eyeColor = dict[@"eye_color"];
    person.birthYear = [dict[@"birth_year"] intValue];
    person.gender = dict[@"gender"];
    
    [self getPersonAvatarData: person.name completion:^(NSData *data) {
        if (data != nil) {
            person.avatarData = data;
            [[[self persistentContainer] viewContext] save: nil];
            [[NSNotificationCenter defaultCenter] postNotificationName: GotAvatarDataNotification object:nil];
        }
    }];
    
    return person;
}

- (void)getPersonAvatarData:(NSString *)name completion:(GetAvatarDataCompletion)completion {
    NSString *nameString = [name stringByReplacingOccurrencesOfString:@" " withString: @""];
    NSString *urlString = [NSString stringWithFormat: @"https://api.adorable.io/avatars/285/%@@adorable.io.png", nameString];
    SDWebImageDownloader *downloader = [SDWebImageDownloader sharedDownloader];
    [downloader downloadImageWithURL:[NSURL URLWithString: urlString] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize, NSURL * _Nullable targetURL) {
        // progression tracking code
    } completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
        if (image && finished) {
            // do something with image
            completion(data);
        }
    }];
}

- (NSArray *)fetchPeople {
    NSManagedObjectContext *context = [[self persistentContainer] viewContext];
    NSFetchRequest *fetchRequest = [Person fetchRequest];
    
    NSArray *objects = [context executeFetchRequest: fetchRequest error: nil];
    
    return objects;
}

- (void)clearData:(NSString *)entityName {
    NSManagedObjectContext *context = [[self persistentContainer] viewContext];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName: entityName];
    
    NSArray *objects = [context executeFetchRequest: fetchRequest error: nil];
    for (NSManagedObject *object in objects) {
        [context deleteObject:object];
    }
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"FSBTask"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

@end
