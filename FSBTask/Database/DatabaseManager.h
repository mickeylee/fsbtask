//
//  DatabaseManager.h
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "Person+CoreDataProperties.h"

#define GotAvatarDataNotification @"GotAvatarDataNotification"

typedef void(^GetAvatarDataCompletion) (NSData *data);

@interface DatabaseManager : NSObject

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (copy, nonatomic) GetAvatarDataCompletion getAvatarCompletionBlock;

+ (DatabaseManager *)sharedInstance;

- (void)saveContext;
- (void)clearData:(NSString *)entityName;

- (Person *)createPersonEntityFrom:(NSDictionary *)dict;
- (NSArray *)fetchPeople;

@end
