//
//  ViewController.m
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import "ViewController.h"
#import "DetailViewController.h"

@interface ViewController ()

@end

@implementation ViewController {
    NSMutableArray *peopleArray;
    int pageIndex;
    BOOL isLoading;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initViewAndData];
    [self fetchFromDB];
    [self getPeopleList];
}

- (void)initViewAndData {
    peopleArray = [[NSMutableArray alloc] init];
    pageIndex = 1;
    isLoading = false;
    
    _indicatorView.hidesWhenStopped = YES;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadTableView) name:GotAvatarDataNotification object:nil];
}

- (void)reloadTableView {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark - GET DATA

- (void)fetchFromDB {
    NSArray *array = [[DatabaseManager sharedInstance] fetchPeople];
    peopleArray = [NSMutableArray arrayWithArray: array];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)getPeopleList {
    [_indicatorView startAnimating];
    isLoading = true;
    
    [[APIManager sharedInstance] listPeople:pageIndex completion:^(NSDictionary *return_dict) {
        [self.indicatorView stopAnimating];
        self->pageIndex += 1;
        self->isLoading = false;
        
        [self createPeopleEntitiesFrom: return_dict];
    } failure:^(NSError *error) {
        [self.indicatorView stopAnimating];
        self->isLoading = false;
        [self showAlertWithTitle:@"Error" message:error.localizedDescription];
    }];
}

- (void)createPeopleEntitiesFrom:(NSDictionary *)dic {
    NSInteger count = [[dic objectForKey:@"count"] integerValue];
    if (count && count > 0) {
        NSArray *result = [dic objectForKey:@"results"];
        
        for (NSDictionary *personDic in result) {
            Person *person = [[DatabaseManager sharedInstance] createPersonEntityFrom: personDic];
            [peopleArray addObject: person];
        }
    }
    
    [[[[DatabaseManager sharedInstance] persistentContainer] viewContext] save: nil];
    [self reloadTableView];
}


#pragma mark - SCROLLVIEW DELEGATE

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([scrollView isKindOfClass:[UITableView class]]) {
        if (isLoading) { return; }
        
        if ((scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height) {
            isLoading = true;
            [self getPeopleList];
        }
    }
}


#pragma mark - TABLEVIEW

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return peopleArray.count;
}

- (CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return 60.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    Person *person = peopleArray[indexPath.row];
    cell.imageView.image = [UIImage imageWithData: person.avatarData];
    cell.textLabel.text = person.name;
    cell.detailTextLabel.text = [NSString stringWithFormat: @"height: %d, mass: %d", person.height, person.mass];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath: indexPath animated: NO];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle: @"" style: UIBarButtonItemStylePlain target: nil action:nil];
    DetailViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier: @"detailVCID"];
    vc.person = peopleArray[indexPath.row];
    [self.navigationController pushViewController: vc animated: YES];
}


#pragma mark - ALERT

- (void)showAlertWithTitle:(NSString *)title message:(NSString *)message {
    if (!title && !message) {
        return;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle: @"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
    }];
    
    [alert addAction:okAction];
    [self presentViewController:alert animated:true completion:nil];
}

@end
