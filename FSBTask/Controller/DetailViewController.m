//
//  DetailViewController.m
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    [self updateView];
}

- (void)updateView {
    self.navigationItem.title = _person.name;
    
    _avatarImageView.image = [UIImage imageWithData: _person.avatarData];
    _nameLabel.text = _person.name;
    _heightLabel.text = [NSString stringWithFormat: @"%d", _person.height];
    _massLabel.text = [NSString stringWithFormat: @"%d", _person.mass];
    _hairLabel.text = _person.hairColor;
    _skinLabel.text = _person.skinColor;
    _eyeLabel.text = _person.eyeColor;
    _birthLabel.text = [NSString stringWithFormat: @"%d", _person.birthYear];
    _genderLabel.text = _person.gender;
}

@end
