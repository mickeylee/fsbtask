//
//  ViewController.h
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APIManager.h"
#import "DatabaseManager.h"

@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;

@end
