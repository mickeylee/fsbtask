//
//  NSString+Codable.h
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URLEncoding)

- (NSString *)URLEncodedString;
- (NSString *)URLDecodedString;

@end
