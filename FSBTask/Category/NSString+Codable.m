//
//  NSString+Codable.m
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import "NSString+Codable.h"

@implementation NSString (URLEncoding)

- (NSString *)URLEncodedString {
    return (NSString *)CFBridgingRelease((__bridge CFTypeRef _Nullable)([[self description] stringByAddingPercentEncodingWithAllowedCharacters: [[NSCharacterSet characterSetWithCharactersInString:@":?=,!$&'()*+;[]@#~"] invertedSet]]));
}

- (NSString *)URLDecodedString {
    return (NSString *)CFBridgingRelease(CFURLCreateStringByReplacingPercentEscapes(kCFAllocatorDefault, (CFStringRef)self, CFSTR("")));
}


@end
