//
//  APIRequest.m
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import "APIRequest.h"
#import "APIManager.h"

@implementation APIRequest

- (id)initWithSession:(NSURLSession *)session Request:(NSMutableURLRequest *)request {
    if (self = [super init]) {
        aRequest = request;
        
        [self initTask: session];
    }
    return self;
}

- (void)initTask:(NSURLSession *)session {
    dataTask = [session dataTaskWithRequest: (NSURLRequest *)aRequest completionHandler: ^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error.code == NSURLErrorTimedOut) {
            // refresh token or rebuild connection
        }
        
        [self processeResult:data Response:response Error:error];
    }];
}


#pragma mark - INTERNAL METHODS

- (void)processeResult:(NSData *)data Response:(NSURLResponse *)response Error:(NSError *)error {
    aResponse = (NSHTTPURLResponse *)response;
    
    if (error) {
        self.error = error;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self requestDidFail];
        });
        
    } else {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSInteger statusCode = [httpResponse statusCode];
        
        if (statusCode == 200) {
            resultData = data;
            [self processResult];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self requestDidFinish];
            });
        } else {
            self.error = [[NSError alloc] initWithDomain:@"" code:statusCode userInfo:[[NSDictionary alloc] init]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self requestDidFail];
            });
        }
    }
}

- (NSString *)resultString {
    return [[NSString alloc] initWithData: resultData encoding: NSUTF8StringEncoding];
}

- (NSObject *)resultJSON {
    return [NSJSONSerialization JSONObjectWithData:resultData options: NSJSONReadingMutableContainers error:nil];
}


#pragma mark - METHODS

- (void)deliver {
    if (dataTask) {
        [dataTask resume];
    }
}

- (void)cancel {
    if (taskMode == APIDefaultTaskMode) {
        [dataTask cancel];
    }
}

- (void)processResult {
    self.result_dict = (NSDictionary *)[self resultJSON];
}

- (void)requestDidFinish {
    [[APIManager sharedInstance] removeRequest: self];
    
    if (self.requestCompletionBlock) {
        self.requestCompletionBlock(self.result_dict);
    }
}

- (void)requestDidFail {
    [[APIManager sharedInstance] removeRequest: self];
    
    if (self.failureBlock) {
        self.failureBlock(self.error);
    }
}

@end
