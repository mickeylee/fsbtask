//
//  APIManager.m
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import "APIManager.h"

static APIManager *sharedInstance = nil;

@implementation APIManager

+ (void)initialize {
    if (sharedInstance == nil) {
        sharedInstance = [[APIManager alloc] init];
    }
}

+ (APIManager *)sharedInstance {
    if (sharedInstance == nil) {
        [self initialize];
    }
    
    return sharedInstance;
}

+ (void)clear {
    sharedInstance = nil;
}

- (id)init {
    if (self = [super init]) {
        _requestQueue = [[NSMutableArray alloc] init];
        
        NSURLSessionConfiguration *defaultConfig = [NSURLSessionConfiguration ephemeralSessionConfiguration];
        defaultConfig.timeoutIntervalForRequest = TIMEOUT_INTERVAL;
        defaultSession = [NSURLSession sessionWithConfiguration:defaultConfig delegate:self delegateQueue: operationQueue];
    }
    
    return self;
}


# pragma mark - REQUEST FUNCTIONS

- (NSString *)httpMethodToString: (APIHttpMethod)method {
    NSString *result = nil;
    
    switch(method) {
        case GET:
            result = @"GET";
            break;
        case POST:
            result = @"POST";
            break;
        case PUT:
            result = @"PUT";
            break;
        default:
            break;
    }
    
    return result;
}

- (NSMutableURLRequest *)requestStringBodyWithPath: (NSString *)path parameters: (NSDictionary *)params headers: (NSDictionary *)headers body: (NSMutableData *)body method: (APIHttpMethod)method {
    NSMutableString *urlString = [NSMutableString stringWithString:path];
    NSString *methodString = [self httpMethodToString: method];
    
    if (method == GET) {
        if (params != nil && [params count] > 0) {
            [urlString appendString:@"?"];
            NSEnumerator *enumerator = params.keyEnumerator;
            NSString *key = [enumerator nextObject];
            while (key) {
                [urlString appendFormat:@"%@=%@", key, [[params objectForKey: key] URLEncodedString]];
                key = [enumerator nextObject];
                if (key) [urlString appendString:@"&"];
            }
        }
    }
    NSURL *url = [NSURL URLWithString: urlString];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL: url cachePolicy: NSURLRequestReloadIgnoringLocalCacheData timeoutInterval: TIMEOUT_INTERVAL];
    [request setHTTPMethod: methodString];
    
    // header
    if (headers != nil) {
        for (NSString *key in headers.allKeys)
            [request setValue: [headers objectForKey: key] forHTTPHeaderField: key];
    }
    // body
    if (body != nil) {
        [request setValue: [NSString stringWithFormat: @"%ld", (long)[body length]] forHTTPHeaderField: @"Content-Length"];
    }
    
    return request;
}

- (void)addRequest:(APIRequest *)task {
    @synchronized(_requestQueue) {
        [_requestQueue addObject:task];
        if (_requestQueue.count <= MAX_CONCURRENT_FEED) {
            [task deliver];
        }
    }
}

- (void)removeRequest:(APIRequest *)task {
    @synchronized(_requestQueue) {
        NSInteger index = [_requestQueue indexOfObject:task];
        if (index != NSNotFound) {
            if (index <= MAX_CONCURRENT_FEED) {
                [_requestQueue removeObject:task];
                if (_requestQueue.count >= MAX_CONCURRENT_FEED) {
                    APIRequest *task = [_requestQueue objectAtIndex:(MAX_CONCURRENT_FEED-1)];
                    [task deliver];
                }
            } else
                [_requestQueue removeObject:task];
        }
    }
}


# pragma mark - CLIENTS

- (void)listPeople:(int)pageIndex completion:(APIRequestCompletionBlock)completion failure: (APIRequestFailureBlock)failure {
    NSString *urlpath = [NSString stringWithFormat:@"%@/%@", BASE_URL, @"people"];
    if (pageIndex > 1) {
        urlpath = [urlpath stringByAppendingString: [NSString stringWithFormat: @"%@%d", @"/?page=", pageIndex]];
    }
    
    NSMutableURLRequest *urlRequest = [self requestStringBodyWithPath: urlpath
                                                           parameters: nil
                                                              headers: nil
                                                                 body: nil
                                                               method: GET];
    
    APIRequest *task = [[APIRequest alloc] initWithSession: defaultSession Request: urlRequest];
    task.requestCompletionBlock = completion;
    task.failureBlock = failure;
    [self addRequest:task];
}

@end
