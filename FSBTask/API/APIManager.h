//
//  APIManager.h
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIRequest.h"
#import "NSString+Codable.h"

#define TIMEOUT_INTERVAL 60.0
#define MAX_CONCURRENT_FEED 5
#define BASE_URL @"https://swapi.co/api"

typedef enum {
    GET = 1,
    POST,
    PUT
} APIHttpMethod;

@interface APIManager : NSObject<NSURLSessionDelegate> {

    NSOperationQueue *operationQueue;
    NSURLSession *defaultSession;

}

@property (strong, nonatomic) NSMutableArray *requestQueue;

+ (APIManager *)sharedInstance;
+ (void)clear;

# pragma mark - REQUESTS
- (void)addRequest:(APIRequest *)request;
- (void)removeRequest:(APIRequest *)request;

# pragma mark - CLIENTS
- (void)listPeople:(int)pageIndex completion:(APIRequestCompletionBlock)completion failure: (APIRequestFailureBlock)failure;

@end
