//
//  APIRequest.h
//  FSBTask
//
//  Created by Mickey on 26/12/2018.
//  Copyright © 2018 Mickey. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    APIDefaultTaskMode = 1,
    APIDownloadTaskMode,
    APIUploadTaskMode
} APITaskMode;

typedef void(^APIRequestCompletionBlock) (NSDictionary *return_dict);
typedef void(^APIRequestFailureBlock) (NSError *error);

@interface APIRequest : NSObject {

    NSURLSessionDataTask *dataTask;
    APITaskMode taskMode;
    
    NSMutableURLRequest *aRequest;
    NSHTTPURLResponse *aResponse;
    NSData *resultData;

}

@property (strong, nonatomic) NSDictionary *result_dict;
@property (strong, nonatomic) NSError *error;
@property (strong, nonatomic) NSObject *resultJSON;
@property (nonatomic, readonly) NSHTTPURLResponse *response;

@property (copy, nonatomic) APIRequestCompletionBlock requestCompletionBlock;
@property (copy, nonatomic) APIRequestFailureBlock failureBlock;

- (id)initWithSession:(NSURLSession *)session Request:(NSMutableURLRequest *)request;

- (void)deliver;
- (void)cancel;

- (void)processResult;
- (void)requestDidFinish;
- (void)requestDidFail;

@end
